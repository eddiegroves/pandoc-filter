import { MetaNode } from "node-pandoc-filter";

export const BASE_PATH_KEY = "basePath";
export const ATTACHMENT_PATH_KEY = "attachmentPath";

export function getBasePath(
  meta: Record<string, MetaNode>
): string | undefined {
  const basePathMeta = meta[BASE_PATH_KEY];
  if (basePathMeta && basePathMeta.t == "MetaString") {
    return basePathMeta.c;
  }
}

export function getAttachmentPath(
  meta: Record<string, MetaNode>
): string | undefined {
  const metaValue = meta[ATTACHMENT_PATH_KEY];
  if (metaValue && metaValue.t == "MetaString") {
    return metaValue.c;
  }
}
