import { NodeType } from "node-pandoc-filter";
import { transformLink } from "./transformLink";

describe("transform links", () => {
  test("should set link url", async () => {
    expect(
      transformLink([["id", [], []], [], ["./file.txt", ""]], "", {
        basePath: { c: "new-path", t: NodeType.MetaString },
      })
    ).toMatchInlineSnapshot(`
      Object {
        "c": Array [
          Array [
            "id",
            Array [],
            Array [],
          ],
          Array [],
          Array [
            "./new-path/file.txt",
            "",
          ],
        ],
        "t": "Link",
      }
    `);
  });

  test("should not change external links", async () => {
    expect(
      transformLink([["id", [], []], [], ["https://example.org", ""]], "", {
        basePath: { c: "new-path", t: NodeType.MetaString },
      })
    ).toMatchInlineSnapshot(`
      Object {
        "c": Array [
          Array [
            "id",
            Array [],
            Array [],
          ],
          Array [],
          Array [
            "https://example.org",
            "",
          ],
        ],
        "t": "Link",
      }
    `);
  });

  test("should fix org attachment links", async () => {
    expect(
      transformLink(
        [
          ["", [], []],
          [{ c: "attachment:attachment.txt", t: NodeType.Str }],
          ["attachment:attachment.txt", ""],
        ],
        "",
        {
          attachmentPath: { c: "attachments/", t: NodeType.MetaString },
          basePath: { c: "new-path", t: NodeType.MetaString },
        }
      )
    ).toMatchInlineSnapshot(`
      Object {
        "c": Array [
          Array [
            "",
            Array [],
            Array [],
          ],
          Array [
            Object {
              "c": "attachment.txt",
              "t": "Str",
            },
          ],
          Array [
            "./new-path/attachments/attachment.txt",
            "",
          ],
        ],
        "t": "Link",
      }
    `);
  });
});
