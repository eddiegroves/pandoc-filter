import { filter as pandocFilter } from "node-pandoc-filter";
import { Document, Visitor } from "node-pandoc-filter/types";
import { MetaString } from "node-pandoc-filter/nodes";
import { transformRawBlock, variables } from "./transformRawBlock";
import { transformImage } from "./transformImage";
import { transformLink } from "./transformLink";
import { ParaTransform } from "./transformPara";

export const defaultVisitor: Visitor = {
  Para: ParaTransform,
  Image: transformImage,
  Link: transformLink,
  RawBlock: transformRawBlock,
};

export async function filter(
  document: Document,
  visitor: Visitor = defaultVisitor
): Promise<Document> {
  await pandocFilter(document, visitor, process.argv[2] ?? "");

  variables.forEach((value, key) => {
    document.meta[key] = MetaString(value);
  });

  // Rerun with new meta variables
  const transformedDoc = await pandocFilter(
    document,
    visitor,
    process.argv[2] ?? ""
  );

  return transformedDoc;
}
