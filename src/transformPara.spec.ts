import { InlineNode } from "node-pandoc-filter";
import { Space, Str } from "node-pandoc-filter/nodes";
import { ParaTransform } from "./transformPara";

describe("transform paragraphs", () => {
  test("should transform single date", async () => {
    const nodes: InlineNode[] = [Str("<2020-01-15"), Space(), Str("Thu>")];
    const result = await ParaTransform(nodes);
    expect(result).toMatchInlineSnapshot(`
      Object {
        "c": Array [
          Object {
            "c": "15 Jan 2020",
            "t": "Str",
          },
        ],
        "t": "Para",
      }
    `);
  });

  test("should transform org mode inactive date", async () => {
    const nodes: InlineNode[] = [Str("[2020-01-15"), Space(), Str("Thu]")];
    const result = await ParaTransform(nodes);
    expect(result).toMatchInlineSnapshot(`
      Object {
        "c": Array [
          Object {
            "c": "15 Jan 2020",
            "t": "Str",
          },
        ],
        "t": "Para",
      }
    `);
  });

  test("should not transform non org mode date", async () => {
    const nodes: InlineNode[] = [Str("2020-01-15")];
    const result = await ParaTransform(nodes);
    expect(result).toMatchInlineSnapshot(`
      Object {
        "c": Array [
          Object {
            "c": "2020-01-15",
            "t": "Str",
          },
        ],
        "t": "Para",
      }
    `);
  });

  test("should transform multiple dates in a paragraph", async () => {
    const nodes: InlineNode[] = [
      Str("dates"),
      Space(),
      Str("in"),
      Space(),
      Str("[2019-12-30"),
      Space(),
      Str("Fri]"),
      Space(),
      Str("paragraph"),
      Space(),
      Str("[2020-01-15"),
      Space(),
      Str("Thu]"),
    ];
    const result = await ParaTransform(nodes);
    expect(result).toMatchInlineSnapshot(`
      Object {
        "c": Array [
          Object {
            "c": "dates",
            "t": "Str",
          },
          Object {
            "t": "Space",
          },
          Object {
            "c": "in",
            "t": "Str",
          },
          Object {
            "t": "Space",
          },
          Object {
            "c": "30 Dec 2019",
            "t": "Str",
          },
          Object {
            "t": "Space",
          },
          Object {
            "c": "paragraph",
            "t": "Str",
          },
          Object {
            "t": "Space",
          },
          Object {
            "c": "15 Jan 2020",
            "t": "Str",
          },
        ],
        "t": "Para",
      }
    `);
  });
});
