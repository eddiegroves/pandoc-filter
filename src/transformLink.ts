import { Attributes, InlineNode, MetaNode, NodeType } from "node-pandoc-filter";
import { Link } from "node-pandoc-filter/nodes";
import { getAttachmentPath, getBasePath } from "./metadata";
import {
  isAttachmentLink,
  replaceAttachmentColon,
  isLocalFileLink,
  addBasePath,
} from "./utils";

export function transformLink(
  node: [
    attributes: Attributes,
    content: InlineNode[],
    url: [value: string, type: string]
  ],
  _: string,
  meta: Record<string, MetaNode>
) {
  const [attributes, content, url] = node;
  let [urlValue] = url;

  const attachmentPath = getAttachmentPath(meta);
  if (attachmentPath && isAttachmentLink(urlValue)) {
    urlValue = replaceAttachmentColon(urlValue, attachmentPath);

    if (content.length && content[0].t === NodeType.Str) {
      content[0].c = replaceAttachmentColon(content[0].c, "");
    }
  }

  const basePath = getBasePath(meta);
  if (basePath && isLocalFileLink(urlValue)) {
    urlValue = addBasePath(basePath, urlValue);
  }

  url[0] = urlValue;
  return Link(attributes, content, url);
}
