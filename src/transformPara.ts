import { Str, Para } from "node-pandoc-filter/nodes";
import { InlineNode } from "node-pandoc-filter";
import format from "date-fns/format";

export async function ParaTransform(node: InlineNode[]) {
  let updatedNodeList = node;
  let length = updatedNodeList.length;

  for (let index = 0; index < length; index++) {
    // Check to see if we've trimmed the list and are now at the new end
    if (index > updatedNodeList.length) break;
    if (updatedNodeList[index] === undefined) break;

    // Str "[2021-02-25",Space,Str "Thu]"
    const firstString = updatedNodeList[index];
    const secondString = updatedNodeList[index + 2];

    if (
      firstString &&
      firstString.t == "Str" &&
      secondString &&
      secondString.t == "Str"
    ) {
      // [2021-02-25 or <2021-02-25
      const startOfDateMatch = firstString.c.match(
        /(\[|\<)(\d{4}\-\d{2}\-\d{2})/
      );
      if (startOfDateMatch == null) continue;

      // Thu] or Thu>
      const endOfDateMatch = secondString.c.match(/[A-Z][a-z][a-z](\]|\>)/);
      if (endOfDateMatch == null) continue;

      // Get date from start of date match
      const date = new Date(startOfDateMatch[2]);
      const replacementDate = Str(format(date, "d MMM yyyy"));

      updatedNodeList = [
        ...updatedNodeList.slice(0, index),
        replacementDate,
        ...updatedNodeList.slice(index + 3, updatedNodeList.length + 1),
      ];

      length = updatedNodeList.length;
    }
  }

  return Para(updatedNodeList);
}
