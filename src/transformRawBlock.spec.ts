import { mapOrgModeDirectives } from "./transformRawBlock";

describe("transform raw block", () => {
  test("captures directives in global variable", async () => {
    const variables = new Map<string, string>();
    mapOrgModeDirectives(["org", "#+PANDOC_BASE_PATH: new-path/"], variables);
    expect(variables).toMatchInlineSnapshot(`
      Map {
        "basePath" => "new-path/",
      }
    `);
  });

  // Skip as the global variables obviously makes this test invalid
  test("ignores non-org format", async () => {
    const variables = new Map<string, string>();
    mapOrgModeDirectives(["something-else", "test"], variables);
    expect(variables).toMatchInlineSnapshot(`Map {}`);
  });
});
