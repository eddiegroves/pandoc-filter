export function orgModeDirectiveKeyToYamlKey(key: string) {
  return key
    .replace(/_/g, " ")
    .toLowerCase()
    .replace(/(?:^\w|[A-Z]|\b\w)/g, (word, index) => {
      return index === 0 ? word.toLowerCase() : word.toUpperCase();
    })
    .replace(/\s+/g, "");
}

export const removeLeadingDotAndOrSlash = (path: string): string => {
  if (path.startsWith("./")) {
    return path.substring(2);
  }

  if (path.startsWith("/")) {
    return path.substring(1);
  }

  return path;
};

export const removeTrailingDot = (path: string): string => {
  if (path.endsWith("/")) {
    return path.slice(0, -1);
  }

  return path;
};

export const addBasePath = (basePath: string, path: string): string => {
  let normalisedBasePath = removeLeadingDotAndOrSlash(basePath);
  normalisedBasePath = removeTrailingDot(normalisedBasePath);
  const normalisedPath = removeLeadingDotAndOrSlash(path);

  return `./${normalisedBasePath}/${normalisedPath}`;
};

export const replaceAttachmentColon = (
  url: string,
  replace: string
): string => {
  return url.replace("attachment:", replace);
};

export const isAttachmentLink = (url: string): boolean =>
  url.startsWith("attachment:");

export const isLocalFileLink = (url: string): boolean =>
  url.indexOf(":") === -1 || isAttachmentLink(url);
