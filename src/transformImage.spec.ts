import { NodeType } from "node-pandoc-filter";
import { transformImage } from "./transformImage";

describe("transform images", () => {
  test("should set image url", async () => {
    expect(
      transformImage([["id", [], []], [], ["./test.png", ""]], "", {
        basePath: { c: "new-path", t: NodeType.MetaString },
      })
    ).toMatchInlineSnapshot(`
      Object {
        "c": Array [
          Array [
            "id",
            Array [],
            Array [],
          ],
          Array [],
          Array [
            "./new-path/test.png",
            "",
          ],
        ],
        "t": "Image",
      }
    `);
  });

  test("should set image url that does not have leading dot slash", async () => {
    expect(
      transformImage([["id", [], []], [], ["test.png", ""]], "", {
        basePath: { c: "new-path", t: NodeType.MetaString },
      })
    ).toMatchInlineSnapshot(`
      Object {
        "c": Array [
          Array [
            "id",
            Array [],
            Array [],
          ],
          Array [],
          Array [
            "./new-path/test.png",
            "",
          ],
        ],
        "t": "Image",
      }
    `);
  });

  test("should update org attachment images", async () => {
    expect(
      transformImage(
        [
          ["", [], []],
          [{ c: "attachment:attachment.png", t: NodeType.Str }],
          ["attachment:attachment.png", ""],
        ],
        "",
        {
          attachmentPath: { c: "attachments/", t: NodeType.MetaString },
          basePath: { c: "new-path", t: NodeType.MetaString },
        }
      )
    ).toMatchInlineSnapshot(`
      Object {
        "c": Array [
          Array [
            "",
            Array [],
            Array [],
          ],
          Array [
            Object {
              "c": "attachment.png",
              "t": "Str",
            },
          ],
          Array [
            "./new-path/attachments/attachment.png",
            "",
          ],
        ],
        "t": "Image",
      }
    `);
  });
});
