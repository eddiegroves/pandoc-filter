import {
  addBasePath,
  isAttachmentLink,
  isLocalFileLink,
  orgModeDirectiveKeyToYamlKey,
  removeLeadingDotAndOrSlash,
  removeTrailingDot,
  replaceAttachmentColon,
} from "./utils";

describe("orgModeDirectiveKeyToYamlKey", () => {
  test.each([
    ["BASE_PATH", "basePath"],
    ["AWESOME", "awesome"],
    ["THREE_GRAND_WORDS", "threeGrandWords"],
  ])("%s -> %s", (orgModeKey, yamlKey) => {
    expect(orgModeDirectiveKeyToYamlKey(orgModeKey)).toBe(yamlKey);
  });
});

describe("removeLeadingDotSlash", () => {
  test.each([
    ["./test", "test"],
    ["test", "test"],
    ["./", ""],
    ["blah./", "blah./"],
    ["som./thing", "som./thing"],
    ["/test", "test"],
    ["//test", "/test"],
  ])("%s -> %s", (path, expected) => {
    expect(removeLeadingDotAndOrSlash(path)).toBe(expected);
  });
});

describe("removeTrailingDot", () => {
  test.each([
    ["./test", "./test"],
    ["test/", "test"],
    ["./", "."],
    ["/", ""],
    ["som/thing", "som/thing"],
    ["/test", "/test"],
  ])("%s -> %s", (path, expected) => {
    expect(removeTrailingDot(path)).toBe(expected);
  });
});

describe("addBasePath", () => {
  test.each([
    ["parent", "child", "./parent/child"],
    ["parent/", "child", "./parent/child"],
    ["./parent/", "child", "./parent/child"],
    ["parent", "child/des/", "./parent/child/des/"],
  ])("%s + %s -> %s", (basePath, path, expected) => {
    expect(addBasePath(basePath, path)).toBe(expected);
  });
});

describe("replaceAttachmentColon", () => {
  test.each([
    ["attachment:something.txt", "something.txt"],
    ["something.txt", "something.txt"],
    ["something/attachment:", "something/"],
  ])("%s + %s -> %s", (path, expected) => {
    expect(replaceAttachmentColon(path, "")).toBe(expected);
  });
});

describe("isAttachmentLink", () => {
  test.each([
    ["attachment:something.txt", true],
    ["something.txt", false],
    ["something/attachment:", false],
  ])("%s + %s -> %s", (path, expected) => {
    expect(isAttachmentLink(path)).toBe(expected);
  });
});

describe("isLocalFileLink", () => {
  test.each([
    ["attachment:something.txt", true],
    ["./something.txt", true],
    ["something/attachment:", false],
    ["https://test.com/", false],
  ])("%s + %s -> %s", (path, expected) => {
    expect(isLocalFileLink(path)).toBe(expected);
  });
});
