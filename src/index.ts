import { Document } from "node-pandoc-filter/types";
import getStdin from "get-stdin";
import { filter } from "./filter";

async function runFilter() {
  const document: Document = JSON.parse(await getStdin());
  const transformed = JSON.stringify(await filter(document));
  console.log(transformed);
}

runFilter();
