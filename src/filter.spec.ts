import { filter } from "./filter";

describe("filter", () => {
  test("should transform pandoc document", async () => {
    const jsonAst: any = {
      "pandoc-api-version": [1, 22],
      meta: {},
      blocks: [
        {
          t: "RawBlock",
          c: ["org", "#+PANDOC_BASE_PATH: my-path/"],
        },
        {
          t: "Para",
          c: [
            {
              t: "Image",
              c: [["", [], []], [], ["an-image.png", ""]],
            },
          ],
        },
      ],
    };

    const doc = await filter(jsonAst);

    expect(doc).toMatchInlineSnapshot(`
      Object {
        "blocks": Array [
          Object {
            "c": Array [
              "org",
              "#+PANDOC_BASE_PATH: my-path/",
            ],
            "t": "RawBlock",
          },
          Object {
            "c": Array [
              Object {
                "c": Array [
                  Array [
                    "",
                    Array [],
                    Array [],
                  ],
                  Array [],
                  Array [
                    "./my-path/an-image.png",
                    "",
                  ],
                ],
                "t": "Image",
              },
            ],
            "t": "Para",
          },
        ],
        "meta": Object {
          "basePath": Object {
            "c": "my-path/",
            "t": "MetaString",
          },
        },
        "pandoc-api-version": Array [
          1,
          22,
        ],
      }
    `);
  });
});
