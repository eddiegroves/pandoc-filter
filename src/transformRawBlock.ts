import { MetaNode } from "node-pandoc-filter";
import { orgModeDirectiveKeyToYamlKey } from "./utils";

// Directives as metadata
// https://pandoc.org/org.html

export const variables: Map<string, string> = new Map();

export function mapOrgModeDirectives(
  node: [format: string, content: string],
  variablesMap: Map<string, string>
) {
  const [format, content] = node;

  // Don't do anything unless the block contains *org* markup.
  if (format != "org") {
    return undefined; // TODO: Does null do something?
  }

  const directiveMatch = content.match(/#\+PANDOC_(\w+):\s*(.+)$/);
  if (directiveMatch == null) {
    return;
  }

  const name = orgModeDirectiveKeyToYamlKey(directiveMatch[1]);
  const value = directiveMatch[2];
  variablesMap.set(name, value);
}

export function transformRawBlock(
  node: [format: string, content: string],
  _: string,
  _meta: Record<string, MetaNode>
) {
  mapOrgModeDirectives(node, variables);
}
