import { exec } from "child_process";

test("e2e", (done) => {
  exec(
    "pandoc e2e/e2e-test.org --filter dist/index.js -t commonmark",
    (_, out) => {
      expect(out).toMatchInlineSnapshot(`
        "# Test file

        The date is 26 Jul 2021 and was 25 Jul 2021

        -   ![](./my-path/attachments/test.png)
        -   ![](./my-path/attachments/test.png)
        "
      `);
      done();
    }
  );
});
