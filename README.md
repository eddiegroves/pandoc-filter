Pandoc Filter

Custom filters for my purposes!

# Install

```sh
npm install
npm run test # Make sure all good
npm run build
```

# Usage

```sh
pandoc <file> --filter dist/index.js
```
